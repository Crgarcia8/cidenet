import axios from "axios";

export default axios.create({
  baseURL: "http://localhost:8090/cidenet-project/api/index.php/",
  headers: {
    "Content-type": "application/json"
  }
});