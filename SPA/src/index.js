import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Switch } from 'react-router-dom';
import Routes from './Routes';
import { SnackbarProvider } from 'notistack';
import { Provider } from 'react-redux';
import store from './Redux/store';
import { TopBar } from './Components/Common';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <SnackbarProvider maxSnack={3} preventDuplicate autoHideDuration={1500} variant='success' anchorOrigin={{ vertical: 'top', horizontal: 'right', }}>
        <TopBar />
        <BrowserRouter>
          <Switch>
            {<Routes />}
          </Switch>
        </BrowserRouter>
      </SnackbarProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
