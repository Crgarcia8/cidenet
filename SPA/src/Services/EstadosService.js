import http from "../http-common";

export const getEstados = () => {
  return http.get("estados");
}