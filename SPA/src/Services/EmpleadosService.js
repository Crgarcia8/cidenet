import http from "../http-common";

export const getEmpleados = () => {
  return http.get("empleados");
}
export const getEmpleadoPorId = (idEmpleado) => {
  return http.get(`empleados/${idEmpleado}`);
}
export const verificarCorreo = (empleadoId, correo) => {
  return http.post(`empleados/existeCorreo/`, { empleadoId, correo });
}
export const saveEmpleado = (empleado) => {
  return http.post("empleados", empleado);
}
export const updateEmpleado = (idEmpleado, empleado) => {
  return http.put(`empleados/${idEmpleado}`, empleado);
}
export const removeEmpleado = (idEmpleado) => {
  return http.delete(`empleados/${idEmpleado}`);
}