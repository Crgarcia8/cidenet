import http from "../http-common";

export const getAreas = () => {
  return http.get("areas");
}