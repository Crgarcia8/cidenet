import http from "../http-common";

export const getPaises = () => {
  return http.get("paises");
}