import http from "../http-common";

export const getTiposDeIdentificacion = () => {
  return http.get("tiposDeIdentificacion");
}