import './Principal.css';

import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Button } from '@material-ui/core'
import { useHistory } from 'react-router';
import { ConsultarEmpleados } from '../../Components/Sections';
import { obtenerTodosEmpleados } from '../../Redux/Actions/EmpleadoActions';
import { asignarNombrePagina } from '../../Redux/Actions/GeneralesActions';

export const Principal = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  useEffect(() => {
    dispatch(obtenerTodosEmpleados());
    dispatch(asignarNombrePagina("Consultar Empleados"));
  }, [])

  const handleOnClick = () => {
    history.push(`/registrar/`)
  }

  return (
    <div className="container">
      <div className="containerPrincipal">
        <Button
          variant="contained"
          color="primary"
          onClick={handleOnClick}
        >
          Registrar Empleado
        </Button>
        <ConsultarEmpleados />
      </div>
    </div>
  );
}