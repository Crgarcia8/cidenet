import './Registrar.css';

import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { RegistrarEmpleado } from '../../Components/Sections';
import { obtenerTodasAreas } from '../../Redux/Actions/AreaActions';
import { obtenerTodosEstados } from '../../Redux/Actions/EstadoActions';
import { obtenerTodosPaises } from '../../Redux/Actions/PaisActions';
import { obtenerTodosTiposDeIdentificacion } from '../../Redux/Actions/TipoDeIdentificacionActions';
import { asignarNombrePagina } from '../../Redux/Actions/GeneralesActions';

export const Registrar = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(asignarNombrePagina("Registrar Empleado"));
    dispatch(obtenerTodasAreas());
    dispatch(obtenerTodosEstados());
    dispatch(obtenerTodosPaises());
    dispatch(obtenerTodosTiposDeIdentificacion());
  }, [])

  return (
    <div className="container">
      <RegistrarEmpleado />
    </div>
  )
}
