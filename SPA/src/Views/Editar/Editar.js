import './Editar.css';

import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';

import { EditarEmpleado } from '../../Components/Sections';
import { obtenerTodasAreas } from '../../Redux/Actions/AreaActions';
import { obtenerEmpleadoPorId } from '../../Redux/Actions/EmpleadoActions';
import { obtenerTodosEstados } from '../../Redux/Actions/EstadoActions';
import { obtenerTodosPaises } from '../../Redux/Actions/PaisActions';
import { obtenerTodosTiposDeIdentificacion } from '../../Redux/Actions/TipoDeIdentificacionActions';
import { asignarNombrePagina } from '../../Redux/Actions/GeneralesActions';

export const Editar = () => {
  let { id: idEmpleado } = useParams();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(asignarNombrePagina("Editar Empleado"));
    dispatch(obtenerTodasAreas());
    dispatch(obtenerTodosEstados());
    dispatch(obtenerTodosPaises());
    dispatch(obtenerTodosTiposDeIdentificacion());
  }, [])

  return (
    <div className="container">
      <EditarEmpleado idEmpleado={idEmpleado} />
    </div>
  )
}
