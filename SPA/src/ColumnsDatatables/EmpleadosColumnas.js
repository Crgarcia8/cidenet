export const vwEmpleadosColumnas = [
  { field: 'empleado_id', headerName: 'Id', width: 70, disableColumnMenu: true, align: 'center' },
  { field: 'tipo_identificacion_nombre', headerName: 'Tipo de Identificación', width: 220, align: 'center' },
  { field: 'empleado_num_identificacion', headerName: 'Documento', width: 150, align: 'center' },
  { field: 'empleado_primer_nombre', headerName: 'Primer Nombre', width: 200, align: 'center' },
  { field: 'empleado_otros_nombre', headerName: 'Otros Nombres', width: 200, align: 'center' },
  { field: 'empleado_primer_apellido', headerName: 'Primer Apellido', width: 180, align: 'center' },
  { field: 'empleado_segundo_apellido', headerName: 'Segundo Apellido', width: 200, align: 'center' },
  { field: 'area_id', headerName: 'Área Id', width: 150, align: 'center', hide: true },
  { field: 'area_nombre', headerName: 'Área', width: 150, align: 'center' },
  { field: 'empleado_correo', headerName: 'Correo', width: 250, align: 'center' },
  { field: 'empleado_fecha_ingreso', headerName: 'Fecha Ingreso', type: 'date', width: 180, align: 'center' },
  { field: 'estado_id', headerName: 'Estado Id', width: 150, align: 'center', hide: true },
  { field: 'estado_nombre', headerName: 'Estado', width: 150, align: 'center' },
  { field: 'pais_id', headerName: 'País Id', width: 150, align: 'center', hide: true },
  { field: 'pais_nombre', headerName: 'País', width: 150, align: 'center' },
  { field: 'empleado_fecha_registro', headerName: 'Fecha Registro', type: 'date', width: 180, align: 'center' },
  { field: 'empleado_fecha_actualizacion', headerName: 'Fecha Actualización', type: 'date', width: 220, align: 'center' }
];