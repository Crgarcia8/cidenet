import { IconButton } from "@material-ui/core";
import { Edit, Delete, QueryBuilder } from '@material-ui/icons';
import * as React from 'react';
import { DataGrid } from '@material-ui/data-grid';

export default function CDatatable(props) {

  const { data, columns } = props;
  const { handleClickEdit, handleClickRemove, handleClickHora } = props;

  const showEdit = handleClickEdit !== undefined;
  const showRemove = handleClickRemove !== undefined;
  const showEntradaSalida = handleClickHora !== undefined;


  /**
   * Esto se hace por que MaterialUI necesita un campo llamado especificamente "id"
   */
  const _data = JSON.parse(JSON.stringify(data));
  _data.forEach(item => item.id = item.empleado_id);



  /**
   * Objeto para adicionar la columna de acciones(Editar, Eliminar)
   */
  const columnActions = {
    field: '',
    headerName: 'Acciones',
    width: 160,
    disableColumnMenu: true,
    renderCell: (params) => {
      return <>
        {showEdit && <IconButton aria-label="edit" onClick={() => handleClickEdit(params.row)}>
          <Edit color="primary" />
        </IconButton>}
        {showRemove && <IconButton aria-label="delete" onClick={() => handleClickRemove(params.row)}>
          <Delete color="secondary" />
        </IconButton>}
        {showEntradaSalida && <IconButton aria-label="time" onClick={() => handleClickHora(params.row)}>
          <QueryBuilder color="primary" />
        </IconButton>}
      </>
    }
  };


  /**
   * Validacion para adicionar o no la columna de acciones
   */
  const columnsMap = [...columns];
  if (handleClickEdit !== undefined || handleClickRemove !== undefined)
    columnsMap.unshift(columnActions)


  return (
    <div >
      <DataGrid
        autoHeight
        density='compact'
        isRowSelectable={false}
        disableSelectionOnClick={true}
        rows={_data}
        columns={columnsMap}
        pageSize={10} />
    </div>
  );
}
