import TextField from '@material-ui/core/TextField';

export default function CTextField(props) {
  const { id, label, value, onChange, required = false, type = "text",
    disabled = false, error = false, helperText, maxLength = 50, fullWidth = false,
    min, max } = props;

  return (
    <TextField
      id={id}
      type={type}
      error={error}
      leng
      helperText={helperText}
      required={required}
      disabled={disabled}
      label={label}
      value={value}
      onChange={onChange}
      fullWidth={fullWidth}
      InputLabelProps={{
        shrink: true
      }}
      inputProps={
        {
          maxLength: maxLength,
          min: min,
          max: max
        }
      }
    />
  )
}
