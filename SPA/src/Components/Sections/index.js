import { ConsultarEmpleados } from './ConsultarEmpleados/ConsultarEmpleados';
import { EditarEmpleado } from './EditarEmpleado/EditarEmpleado';
import RegistrarEmpleado from './RegistrarEmpleado/RegistrarEmpleado';

export {
  ConsultarEmpleados,
  EditarEmpleado,
  RegistrarEmpleado
}