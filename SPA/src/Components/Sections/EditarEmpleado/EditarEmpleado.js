import './EditarEmpleado.css';

import moment from 'moment';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import Select from '@material-ui/core/Select';
import { useSnackbar } from 'notistack';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';

import { obtenerEmpleadoPorId } from '../../../Redux/Actions/EmpleadoActions';
import { CTextField } from '../../Common';
import { actualizarEmpleado } from '../../../Redux/Actions/EmpleadoActions';

export const EditarEmpleado = ({ idEmpleado }) => {
  const history = useHistory();

  const { enqueueSnackbar } = useSnackbar();

  const dispatch = useDispatch();

  const areas = useSelector(state => state.areas);
  const paises = useSelector(state => state.paises);
  const estados = useSelector(state => state.estados);
  const tiposDeIdentificacion = useSelector(state => state.tiposDeIdentificacion);
  const empleadoSeleccionado = useSelector(state => state.empleadoSeleccionado);

  const [empleado, setEmpleado] = useState({});
  const [disabledButtonEditar, setDisabledButtonEditar] = useState(false);

  useEffect(() => {
    dispatch(obtenerEmpleadoPorId(idEmpleado));//Se ejecuta el evento para consultar el empleado
  }, [])

  useEffect(() => {
    setEmpleado(empleadoSeleccionado);//Se guarda en un estado local del componente para poder modificarlo.
  }, [empleadoSeleccionado])

  if (!idEmpleado) return <div>Por favor seleccione un empleado para editar</div>;

  const handleOnClickEditar = async () => {
    setDisabledButtonEditar(true)
    dispatch(actualizarEmpleado(idEmpleado, empleado))
      .then(() => {
        enqueueSnackbar("La información fue actualizada con éxito");
      })
      .catch(err => {
        enqueueSnackbar("Se ha generado un error", { variant: "error" });
      })
      .finally(() => {
        setDisabledButtonEditar(false);
      });
  }

  const handleChangePais = (e) => setEmpleado({ ...empleado, pais_id: e.target.value });
  const handleChangeTipoDeIdentificacion = (e) => setEmpleado({ ...empleado, tipo_identificacion_id: e.target.value });
  const handleChangeArea = (e) => setEmpleado({ ...empleado, area_id: e.target.value });
  const handleChangeEstado = (e) => setEmpleado({ ...empleado, estado_id: e.target.value });

  const showTiposDeIdentificacion = () => (
    tiposDeIdentificacion.map(item => (
      <MenuItem value={item.tipo_identificacion_id}>{item.tipo_identificacion_nombre}</MenuItem>)
    )
  )
  const showPaises = () => (
    paises.map(item => (
      <MenuItem value={item.pais_id}>{item.pais_nombre}</MenuItem>)
    )
  )
  const showAreas = () => (
    areas.map(item => (
      <MenuItem value={item.area_id}>{item.area_nombre}</MenuItem>)
    )
  )
  const showEstados = () => (
    estados.map(item => (
      <MenuItem value={item.estado_id}>{item.estado_nombre}</MenuItem>)
    )
  )
  const onChangeText = (value, field) => setEmpleado({ ...empleado, [field]: value });

  const handleOnClickCancelar = () => history.push("/");

  const fechaMinimaIngreso = moment(new Date()).subtract('1', 'month').format("yyyy-MM-DD");
  const fechaMaximaIngreso = moment(new Date()).format("yyyy-MM-DD");


  return (
    <Paper elevation={3} className="registrarEmpleado">
      <form noValidate autoComplete="off" className="form">
        <CTextField
          id="idEmpleado"
          label="Id"
          fullWidth
          value={empleado.empleado_id}
          disabled={true}
        />
        <CTextField
          id="primerNombre"
          label="Primer Nombre"
          required
          fullWidth
          maxLength={20}
          value={empleado.empleado_primer_nombre}
          onChange={(e) => onChangeText(e.target.value, "empleado_primer_nombre")}
        />
        <CTextField
          id="otrosNombre"
          label="Otros Nombres"
          required
          fullWidth
          maxLength={20}
          value={empleado.empleado_otros_nombre}
          onChange={(e) => onChangeText(e.target.value, "empleado_otros_nombre")}
        />
        <CTextField
          id="primerApellido"
          label="Primer Apellido"
          required
          fullWidth
          maxLength={20}
          value={empleado.empleado_primer_apellido}
          onChange={(e) => onChangeText(e.target.value, "empleado_primer_apellido")}
        />
        <CTextField
          id="segundoApellido"
          label="Segundo Apellido"
          required
          fullWidth
          maxLength={20}
          value={empleado.empleado_segundo_apellido}
          onChange={(e) => onChangeText(e.target.value, "empleado_segundo_apellido")}
        />
        <CTextField
          id="documento"
          label="Documento"
          required
          fullWidth
          maxLength={20}
          value={empleado.empleado_num_identificacion}
          onChange={(e) => onChangeText(e.target.value, "empleado_num_identificacion")}
        />
        <CTextField
          id="email"
          label="Correo Electrónico"
          type="email"
          fullWidth
          value={empleado.empleado_correo}
          disabled={true}
        />
        <CTextField
          id="fechaIngreso"
          label="Fecha de Ingreso"
          type="date"
          required
          fullWidth
          min={fechaMinimaIngreso}
          max={fechaMaximaIngreso}
          value={empleado.empleado_fecha_ingreso}
          onChange={(e) => onChangeText(e.target.value, "empleado_fecha_ingreso")}
        />
        <FormControl fullWidth>
          <InputLabel shrink id="areaLabel">Área</InputLabel>
          <Select
            id="areaSelect"
            value={empleado.area_id || ''}
            onChange={handleChangeArea}
          >
            {showAreas()}
          </Select>
        </FormControl>
        <FormControl fullWidth>
          <InputLabel shrink id="estadoLabel">Estado</InputLabel>
          <Select
            id="estadoSelect"
            value={empleado.estado_id || ''}
            onChange={handleChangeEstado}
            disabled={true}
          >
            {showEstados()}
          </Select>
        </FormControl>
        <FormControl fullWidth>
          <InputLabel shrink id="paisLabel">País</InputLabel>
          <Select
            id="paisSelect"
            value={empleado.pais_id || ''}
            onChange={handleChangePais}
          >
            {showPaises()}
          </Select>
        </FormControl>
        <FormControl fullWidth>
          <InputLabel shrink id="tipoDeIdentificacionLabel">Tipo de Identificación</InputLabel>
          <Select
            id="tipoDeIdentificacionSelect"
            value={empleado.tipo_identificacion_id || ''}
            onChange={handleChangeTipoDeIdentificacion}
          >
            {showTiposDeIdentificacion()}
          </Select>
        </FormControl>
        <CTextField
          id="fechaActualizacion"
          label="Fecha de Actualización"
          disabled={true}
          fullWidth
          value={empleado.empleado_fecha_actualizacion}
        />
        <CTextField
          id="fechaRegistro"
          label="Fecha de Registro"
          disabled={true}
          fullWidth
          value={empleado.empleado_fecha_registro}
        />
        <Button
          color="secondary"
          onClick={handleOnClickCancelar}
        >
          Cancelar
        </Button>
        <Button
          variant="contained"
          color="primary"
          onClick={handleOnClickEditar}
          disabled={disabledButtonEditar}
        >
          Editar
        </Button>
      </form>
    </Paper>
  )
}
