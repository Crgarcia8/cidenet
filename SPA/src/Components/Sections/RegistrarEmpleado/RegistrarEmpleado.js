import './RegistrarEmpleado.css';

import moment from 'moment';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import Select from '@material-ui/core/Select';
import { useSnackbar } from 'notistack';
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';

import { CTextField } from '../../Common';
import { guardarEmpleado } from '../../../Redux/Actions/EmpleadoActions';
import renderTextField from '../../Common/CTextField/CTextField';
import { validateName, validateDocument } from '../../../GeneralFunctions/GenerateFunctions';

const RegistrarEmpleado = () => {
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();

  const dispatch = useDispatch();

  const areas = useSelector(state => state.areas);
  const paises = useSelector(state => state.paises);
  const estados = useSelector(state => state.estados);
  const tiposDeIdentificacion = useSelector(state => state.tiposDeIdentificacion);

  const [empleado, setEmpleado] = useState({});
  const [disabledButtonRegistrar, setDisabledButtonRegistrar] = useState(false);

  useEffect(() => {
    if (areas[0] && paises[0] && estados[0] && tiposDeIdentificacion[0]) {
      setEmpleado({
        area_id: areas[0] ? areas[0].area_id : '',
        pais_id: paises[0] ? paises[0].pais_id : '',
        estado_id: estados[0] ? estados[0].estado_id : '',
        tipo_identificacion_id: tiposDeIdentificacion[0] ? tiposDeIdentificacion[0].tipo_identificacion_id : '',
      });
    }

  }, [areas, paises, estados, tiposDeIdentificacion])

  const registrarEmpleado = async () => {
    setDisabledButtonRegistrar(true)



    dispatch(guardarEmpleado(empleado))
      .then((response) => {
        enqueueSnackbar("El empleado fue registrado correctamente");
        history.push(`/editar/${response.empleado_id}`)
      })
      .catch(err => {
        enqueueSnackbar("Se ha generado un error", { variant: "error" });
      })
      .finally(() => {
        setDisabledButtonRegistrar(false);
      });
  }

  const handleChangePais = (e) => setEmpleado({ ...empleado, pais_id: e.target.value });
  const handleChangeTipoDeIdentificacion = (e) => setEmpleado({ ...empleado, tipo_identificacion_id: e.target.value });
  const handleChangeArea = (e) => setEmpleado({ ...empleado, area_id: e.target.value });
  const handleChangeEstado = (e) => setEmpleado({ ...empleado, estado_id: e.target.value });

  const showTiposDeIdentificacion = () => (
    tiposDeIdentificacion.map(item => (
      <MenuItem value={item.tipo_identificacion_id}>{item.tipo_identificacion_nombre}</MenuItem>)
    )
  )
  const showPaises = () => (
    paises.map(item => (
      <MenuItem value={item.pais_id}>{item.pais_nombre}</MenuItem>)
    )
  )
  const showAreas = () => (
    areas.map(item => (
      <MenuItem value={item.area_id}>{item.area_nombre}</MenuItem>)
    )
  )
  const showEstados = () => (
    estados.map(item => (
      <MenuItem value={item.estado_id}>{item.estado_nombre}</MenuItem>)
    )
  )
  const onChangeText = (value, field) => setEmpleado({ ...empleado, [field]: value })
  const handleOnClickCancelar = () => history.push("/");
  const handleSubmit = (event) => {
    event.preventDefault();

    const nombre = validateName(empleado.empleado_primer_nombre);
    const otros_nombres = empleado.empleado_otros_nombre ? validateName(empleado.empleado_otros_nombre) : true;
    const primer_apellido = validateName(empleado.empleado_primer_apellido);
    const segundo_apellido = validateName(empleado.empleado_segundo_apellido);
    const documento = validateDocument(empleado.empleado_num_identificacion);
    const fecha_ingreso = empleado.empleado_fecha_ingreso !== undefined ? moment(empleado.empleado_fecha_ingreso).isValid() : false;

    if (nombre && otros_nombres && primer_apellido && segundo_apellido && documento && fecha_ingreso) {
      registrarEmpleado();
    } else {
      enqueueSnackbar("Por Favor ingresar los valores correctamente", { variant: "warning" });
    }
  }
  const fechaMinimaIngreso = moment(new Date()).subtract('1', 'month').format("yyyy-MM-DD");
  const fechaMaximaIngreso = moment(new Date()).format("yyyy-MM-DD");

  return (
    <Paper elevation={3} className="registrarEmpleado">
      <form noValidate autoComplete="off" className="form" onSubmit={handleSubmit}>
        <CTextField
          id="primerApellido"
          label="Primer Apellido"
          required
          fullWidth
          value={empleado.empleado_primer_apellido}
          onChange={(e) => onChangeText(e.target.value, "empleado_primer_apellido")}
          maxLength={20}
        />
        <CTextField
          id="segundoApellido"
          label="Segundo Apellido"
          required
          fullWidth
          value={empleado.empleado_segundo_apellido}
          onChange={(e) => onChangeText(e.target.value, "empleado_segundo_apellido")}
          maxLength={20}
        />
        <CTextField
          id="primerNombre"
          label="Primer Nombre"
          required
          fullWidth
          value={empleado.empleado_primer_nombre}
          onChange={(e) => onChangeText(e.target.value, "empleado_primer_nombre")}
          maxLength={20}
        />
        <CTextField
          id="otrosNombre"
          label="Otros Nombres"
          fullWidth
          value={empleado.empleado_otros_nombre}
          onChange={(e) => onChangeText(e.target.value, "empleado_otros_nombre")}
          maxLength={50}
        />
        <CTextField
          id="documento"
          label="Documento"
          fullWidth
          value={empleado.empleado_num_identificacion}
          onChange={(e) => onChangeText(e.target.value, "empleado_num_identificacion")}
          maxLength={20}
        />
        <CTextField
          id="email"
          label="Correo Electrónico"
          type="email"
          fullWidth
          value={empleado.empleado_correo}
          disabled={true}
          maxLength={300}
        />
        <CTextField
          id="fechaIngreso"
          label="Fecha de Ingreso"
          type="date"
          fullWidth
          value={empleado.empleado_fecha_ingreso}
          onChange={(e) => onChangeText(e.target.value, "empleado_fecha_ingreso")}
          min={fechaMinimaIngreso}
          max={fechaMaximaIngreso}
        />
        <FormControl fullWidth>
          <InputLabel shrink id="areaLabel">Área</InputLabel>
          <Select
            id="areaSelect"
            value={empleado.area_id || ''}
            onChange={handleChangeArea}
          >
            {showAreas()}
          </Select>
        </FormControl>
        <FormControl fullWidth>
          <InputLabel shrink id="estadoLabel">Estado</InputLabel>
          <Select
            id="estadoSelect"
            value={empleado.estado_id || ''}
            onChange={handleChangeEstado}
            disabled={true}
          >
            {showEstados()}
          </Select>
        </FormControl>
        <FormControl fullWidth>
          <InputLabel shrink id="paisLabel">País</InputLabel>
          <Select
            id="paisSelect"
            value={empleado.pais_id || ''}
            onChange={handleChangePais}
          >
            {showPaises()}
          </Select>
        </FormControl>
        <FormControl fullWidth>
          <InputLabel shrink id="tipoDeIdentificacionLabel">Tipo de Identificación</InputLabel>
          <Select
            id="tipoDeIdentificacionSelect"
            value={empleado.tipo_identificacion_id || ''}
            onChange={handleChangeTipoDeIdentificacion}
          >
            {showTiposDeIdentificacion()}
          </Select>
        </FormControl>
        <Button
          color="secondary"
          onClick={handleOnClickCancelar}
        >
          Cancelar
        </Button>
        <Button
          variant="contained"
          color="primary"
          type="submit"
        >
          Registrar
        </Button>
      </form>
    </Paper>
  )
}
export default RegistrarEmpleado;