import './ConsultarEmpleados.css';

import Paper from '@material-ui/core/Paper';
import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import { useSnackbar } from 'notistack';

import { vwEmpleadosColumnas } from '../../../ColumnsDatatables/EmpleadosColumnas';
import { filtrarEmpleados, eliminarEmpleado } from '../../../Redux/Actions/EmpleadoActions';
import { CDatatable, CTextField } from '../../Common';
import { CDialog } from '../../Common';

export const ConsultarEmpleados = () => {
  const history = useHistory();
  const { enqueueSnackbar } = useSnackbar();
  const dispatch = useDispatch();

  const empleados = useSelector(state => state.empleados.data);
  const empleadosFiltros = useSelector(state => state.empleados.filtros);
  const empleadosFiltrados = useSelector(state => state.empleados.dataFiltrada);

  const [buscar, setBuscar] = useState({})
  const [modalOpen, setModalOpen] = useState(false);
  const [modalOpenHoraSalida, setModalOpenHoraSalida] = useState(false);

  const [empleadoSeleccionado, setEmpleadoSeleccionado] = useState({});

  const handleClickEdit = (row) => {
    history.push(`/editar/${row.empleado_id}`)
  }

  const handleClickRemove = (row) => {
    setEmpleadoSeleccionado(row);
    setModalOpen(true);
  }
  const handleClickHora = () => {
    // setModalOpenHoraSalida(true)
  }


  const onChangeText = (value, field) => {
    const filtros = { ...buscar, [field]: value };
    setBuscar(filtros);
    dispatch(filtrarEmpleados(filtros));
  }

  const handleOnRemove = () => {
    dispatch(eliminarEmpleado(empleadoSeleccionado.empleado_id))
      .then(() => {
        setModalOpen(false);
        enqueueSnackbar("El empleado fue eliminado con éxito");
      })
      .catch(err => {
        enqueueSnackbar("Se ha generado un error", { variant: "error" });
      })
  }
  const handleOnHoraSalida = () => {

  }

  const isEmptyFilter = !Object.keys(empleadosFiltros).length;
  return (
    <>
      <Paper elevation={3} className="containterFiltros">
        <CTextField
          id="primerNombre"
          label="Primer Nombre"
          maxLength={20}
          value={buscar.empleado_primer_nombre || ''}
          onChange={(e) => onChangeText(e.target.value, "empleado_primer_nombre")}
        />
        <CTextField
          id="otrosNombres"
          label="Otros Nombres"
          maxLength={20}
          value={buscar.empleado_otros_nombre || ''}
          onChange={(e) => onChangeText(e.target.value, "empleado_otros_nombre")}
        />
        <CTextField
          id="primerApellido"
          label="Primer Apellido"
          maxLength={20}
          value={buscar.empleado_primer_apellido || ''}
          onChange={(e) => onChangeText(e.target.value, "empleado_primer_apellido")}
        />
        <CTextField
          id="segundoApellido"
          label="Segundo Apellido"
          maxLength={20}
          value={buscar.empleado_segundo_apellido || ''}
          onChange={(e) => onChangeText(e.target.value, "empleado_segundo_apellido")}
        />
        <CTextField
          id="documento"
          label="Documento"
          maxLength={20}
          value={buscar.empleado_num_identificacion || ''}
          onChange={(e) => onChangeText(e.target.value, "empleado_num_identificacion")}
        />
        <CTextField
          id="correo"
          label="Correo"
          maxLength={300}
          value={buscar.empleado_correo || ''}
          onChange={(e) => onChangeText(e.target.value, "empleado_correo")}
        />
      </Paper>
      <Paper elevation={3} >
        <CDatatable
          columns={vwEmpleadosColumnas}
          data={isEmptyFilter ? empleados : empleadosFiltrados}
          handleClickEdit={handleClickEdit}
          handleClickRemove={handleClickRemove}
          handleClickHora={handleClickHora}
        />
      </Paper>
      <CDialog
        open={modalOpen}
        title={`Confirmación para eliminar empleado?`}
        description={`¿Está seguro de que desea eliminar el empleado: ${empleadoSeleccionado.empleado_primer_nombre} ${empleadoSeleccionado.empleado_primer_apellido}?`}
        onClose={() => { setModalOpen(false) }}
        onSubmit={handleOnRemove}
      />
      <CDialog
        open={modalOpenHoraSalida}
        title={`Registrar ingreso o salida?`}
        description={`Registrar ingreso o salida del empleado: ${empleadoSeleccionado.empleado_primer_nombre} ${empleadoSeleccionado.empleado_primer_apellido}?`}
        onClose={() => { setModalOpenHoraSalida(false) }}
        showFooter={false}
      >

      </CDialog>
    </>
  )
}
