import React from 'react';
import { Route, Redirect } from "react-router";
import { Principal } from './Views/Principal/Principal'
import { Editar } from './Views/Editar/Editar'
import { Registrar } from './Views/Registrar/Registrar'

export default function Routes() {
  return (
    <>
      <Route exact path="/">
        <Redirect to="/empleados" />
      </Route>
      <Route path="/empleados" component={Principal} />
      <Route path="/editar/:id" component={Editar} />
      <Route path="/registrar" component={Registrar} />
    </>
  )
}

