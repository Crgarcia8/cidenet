
import { verificarCorreo } from '../Services/EmpleadosService';

/**
 * Funcion recursiva para obtener el correo único verificando en la base de datos.
 * @param {object} empleado Informacion del empleado
 * @param {number} [empleado.idEmpleado] Id Del empleado.
 * @param {string} empleado.nombre Nombre Del empleado.
 * @param {string} empleado.apellido Apellido Del empleado.
 * @param {string} empleado.dominio Dominio del correo del empleado
 * @param {string} [empleado.secuencia] Secuencia para aumentar un numero en caso de que exista el correo.
 * 
 * @returns Correo generado
 */
export const generateEmail = async ({ idEmpleado = -1, nombre, apellido, dominio, secuencia = 0 }) => {
  let contador = 0;
  let again = false;

  let correo = `${nombre}.${apellido}`;
  if (secuencia > 0) {
    correo = `${correo}.${secuencia}`;
  }
  correo = correo + `@${dominio}`;

  try {
    await verificarCorreo(idEmpleado, correo);
  } catch (e) {
    again = true;
    if (secuencia > 0)
      contador = contador + secuencia;
  }
  if (again)
    correo = await generateEmail(
      {
        idEmpleado,
        nombre,
        apellido,
        dominio,
        secuencia: contador + 1
      });
  return correo;
}

/**
 * Funcion para validar que una palabra permita:
 *  Carácteres de la A a la Z en Minuscula y/o Mayuscula
 *  Permita espacios
 *  No permita caracteres especiales
 * 
 * @param {string} texto Texto a validar 
 * @returns Verdadero si es correcto, falso si no es correcto.
 */
export const validateName = (texto) => texto !== undefined ? /^[a-zA-Z ]+$/.test(texto) : false;

/**
 * Funcion para validar el documento del empleado, la cual permite:
 *  Carácteres de la A a la Z en Minuscula y/o Mayuscula
 *  Números del 0 al 9
 *  Carácter(-)
 * 
 * @param {string} texto Texto a validar 
 * @returns Verdadero si es correcto, falso si no es correcto.
 */
export const validateDocument = (texto) => texto !== undefined ? /^[a-zA-Z0-9-]+$/.test(texto) : false;
