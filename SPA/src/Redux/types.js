/**
 * Types de los empleados
 */
export const GET_EMPLEADOS = "GET_EMPLEADOS";
export const SET_EMPLEADO_SELECCIONADO = "SET_EMPLEADO_SELECCIONADO";
export const FILTRAR_EMPLEADOS = "FILTRAR_EMPLEADOS";
export const REMOVE_EMPLEADO = "REMOVE_EMPLEADO";



/**
 * Types de las Areas
 */
export const GET_AREAS = "GET_AREAS";

/**
* Types de los paises
*/
export const GET_PAISES = "GET_PAISES";

/**
* Types de los estados de los empleados
*/
export const GET_ESTADOS = "GET_ESTADOS";

/**
* Types de los tipos de identificacion
*/
export const GET_TIPOS_DE_IDENTIFICACION = "GET_TIPOS_DE_IDENTIFICACION";

/**
* Types del titulo de la página
*/
export const GET_TITLE_PAGE = "GET_TITLE_PAGE";

