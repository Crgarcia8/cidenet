import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import thunk from 'redux-thunk';
import { getTodosEmpleados, getEmpleadoSeleccionado } from "./Reducers/EmpleadoReducer";
import { getTodasAreas } from "./Reducers/AreaReducer";
import { getTodosEstados } from "./Reducers/EstadoReducer";
import { getTodasPaises } from "./Reducers/PaisReducer";
import { getTodosTiposDeIdentificacion } from "./Reducers/TipoDeIdentificacionReducer";
import { getTitlePage } from "./Reducers/GeneralesReducer";


const enhancers = [];
const isDevelopment = process.env.NODE_ENV === 'development';
if (isDevelopment && typeof window !== 'undefined' && window.devToolsExtension) {
    enhancers.push(window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
}
const store = createStore(combineReducers({

    /**Reducers de los empleados */
    empleados: getTodosEmpleados,
    empleadoSeleccionado: getEmpleadoSeleccionado,

    /**Reducers de listas genericas */
    areas: getTodasAreas,
    estados: getTodosEstados,
    paises: getTodasPaises,
    tiposDeIdentificacion: getTodosTiposDeIdentificacion,

    /**Reducers Generales */
    titlePage: getTitlePage

}), compose(applyMiddleware(thunk), ...enhancers),
);
export default store;