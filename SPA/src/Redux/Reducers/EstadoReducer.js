import { GET_ESTADOS } from "../types";

export const getTodosEstados = (state = [], { payload, type }) => {
    switch (type) {
        case GET_ESTADOS:
            return payload;
        default:
            return state;
    }
};