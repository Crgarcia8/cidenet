import { GET_TIPOS_DE_IDENTIFICACION } from "../types";

export const getTodosTiposDeIdentificacion = (state = [], { payload, type }) => {
    switch (type) {
        case GET_TIPOS_DE_IDENTIFICACION:
            return payload;
        default:
            return state;
    }
};