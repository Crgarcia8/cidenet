import { GET_TITLE_PAGE } from "../types";

export const getTitlePage = (state = "Principal", { payload, type }) => {
    switch (type) {
        case GET_TITLE_PAGE:
            return payload;
        default:
            return state;
    }
};