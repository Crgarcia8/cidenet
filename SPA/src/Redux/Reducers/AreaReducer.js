import { GET_AREAS } from "../types";

export const getTodasAreas = (state = [], { payload, type }) => {
    switch (type) {
        case GET_AREAS:
            return payload;
        default:
            return state;
    }
};