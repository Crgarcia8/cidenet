import { GET_PAISES } from "../types";

export const getTodasPaises = (state = [], { payload, type }) => {
    switch (type) {
        case GET_PAISES:
            return payload;
        default:
            return state;
    }
};