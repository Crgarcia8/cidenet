import { GET_EMPLEADOS, FILTRAR_EMPLEADOS, SET_EMPLEADO_SELECCIONADO, REMOVE_EMPLEADO } from "../types";

const empleadosEstadoInicial = {
    data: [],
    dataFiltrada: [],
    filtros: {}
}
export const getTodosEmpleados = (state = empleadosEstadoInicial, { payload, type }) => {
    switch (type) {
        case GET_EMPLEADOS:
            return { data: payload, dataFiltrada: [], filtros: {} };
        case FILTRAR_EMPLEADOS:
            let dataFilter = [...state.data];


            /**Ciclo para recorrer todos los filtros dinamicamente */
            Object.entries(payload).forEach(filtro => {

                /**Ciclo para filtrar los empleados */
                dataFilter = dataFilter.filter(empleado => {

                    /**Condicion para verificar que tenga un texto valido */
                    if (empleado[filtro[0]]) {
                        /**Retorna true si encontró el texto dentro del objeto del empleado */
                        return empleado[filtro[0]].toLocaleLowerCase().includes(payload[filtro[0]].toLocaleLowerCase());
                    }

                    return false;
                })
            })
            return { ...state, dataFiltrada: dataFilter, filtros: payload };
        case REMOVE_EMPLEADO:
            let empleados = [...state.data];

            /**Logica para remover un item del listado de empleados */
            const index = empleados.findIndex(item => item.empleado_id === payload);
            empleados.splice(index, 1);

            return { data: empleados, dataFiltrada: [], filtros: {} };
        default:
            return state;
    }
};
export const getEmpleadoSeleccionado = (state = {}, { payload, type }) => {
    switch (type) {
        case SET_EMPLEADO_SELECCIONADO:
            return payload;
        default:
            return state;
    }
};