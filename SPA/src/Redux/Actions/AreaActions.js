import { GET_AREAS } from "../types";
import { getAreas } from '../../Services/AreasService';

/**Consultar para obtener todas las areas */
export const obtenerTodasAreas = () => async dispatch => {
    const response = await getAreas();
    return dispatch(recibirAreas(response.data));
}


/**Objetos para devolver al store */
const recibirAreas = (areas) => ({
    type: GET_AREAS,
    payload: areas
})
/**Objetos para devolver al store */