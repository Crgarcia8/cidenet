import { GET_ESTADOS } from "../types";
import { getEstados } from '../../Services/EstadosService';

/**Consultar para obtener todos los estados */
export const obtenerTodosEstados = () => async dispatch => {
    const response = await getEstados();
    return dispatch(recibirEstados(response.data));
}


/**Objetos para devolver al store */
const recibirEstados = (estados) => ({
    type: GET_ESTADOS,
    payload: estados
})
/**Objetos para devolver al store */