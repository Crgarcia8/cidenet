import { GET_TIPOS_DE_IDENTIFICACION } from "../types";
import { getTiposDeIdentificacion } from '../../Services/TiposDeIdentificacionService';

/**Consultar para obtener todos los Tipos de Identificacion */
export const obtenerTodosTiposDeIdentificacion = () => async dispatch => {
    const response = await getTiposDeIdentificacion();
    return dispatch(recibirTiposDeIdentificacion(response.data));
}


/**Objetos para devolver al store */
const recibirTiposDeIdentificacion = (tiposDeIdentificacion) => ({
    type: GET_TIPOS_DE_IDENTIFICACION,
    payload: tiposDeIdentificacion
})
/**Objetos para devolver al store */