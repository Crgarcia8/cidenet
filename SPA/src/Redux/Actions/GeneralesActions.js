import { GET_TITLE_PAGE } from "../types";

/**Asignar el nombre de la página */
export const asignarNombrePagina = (titulo) => async dispatch => {
    dispatch(asignarTituloPagina(titulo));
}


/**Objetos para devolver al store */
const asignarTituloPagina = (titulo) => ({
    type: GET_TITLE_PAGE,
    payload: titulo
})
/**Objetos para devolver al store */