import { GET_EMPLEADOS, FILTRAR_EMPLEADOS, SET_EMPLEADO_SELECCIONADO, REMOVE_EMPLEADO } from "../types";
import { getEmpleadoPorId, getEmpleados, saveEmpleado, updateEmpleado, removeEmpleado } from '../../Services/EmpleadosService';
import { generateEmail } from '../../GeneralFunctions/GenerateFunctions';

/**Acción para  obtener todos los empleados */
export const obtenerTodosEmpleados = () => async dispatch => {
    const response = await getEmpleados();
    return dispatch(recibirEmpleados(response.data));
}

/**Acción para filtrar los empleados con filtros dinamicos */
export const filtrarEmpleados = (filtros) => dispatch => {
    return dispatch(recibirFiltroEmpleados(filtros));
}

/**Acción para obtener el empleado por idEmpleado*/
export const obtenerEmpleadoPorId = (idEmpleado) => async dispatch => {
    const response = await getEmpleadoPorId(idEmpleado);
    dispatch(setEmpleadoSeleccionado(response.data));
}
/**Acción para asignar al store el empleado seleccionado */
export const seleccionarEmpleado = (empleado) => async dispatch => {
    dispatch(setEmpleadoSeleccionado(empleado));
}

/**Acción para crear un nuevo empelado. */
export const guardarEmpleado = (empleado) => async dispatch => {
    const correo = await crearEmail(empleado);
    const { data } = await saveEmpleado({ ...empleado, empleado_correo: correo });
    dispatch(setEmpleadoSeleccionado(data));
    return data;
}

/**Acción para actualizar un empleado. */
export const actualizarEmpleado = (idEmpleado, empleado) => async dispatch => {
    const correo = await crearEmail(empleado, idEmpleado);
    const { data } = await updateEmpleado(idEmpleado, { ...empleado, empleado_correo: correo });
    dispatch(setEmpleadoSeleccionado(data));
}

/**Acción para eliminar un empleado */
export const eliminarEmpleado = (idEmpleado) => async dispatch => {
    await removeEmpleado(idEmpleado);
    dispatch(removerEmpleadoPorId(idEmpleado));
}

/**Objetos para devolver al store */
const recibirEmpleados = (empleados) => ({
    type: GET_EMPLEADOS,
    payload: empleados
})
const setEmpleadoSeleccionado = (empleado) => ({
    type: SET_EMPLEADO_SELECCIONADO,
    payload: empleado
})
const recibirFiltroEmpleados = (filtros) => ({
    type: FILTRAR_EMPLEADOS,
    payload: filtros
})
const removerEmpleadoPorId = (idEmpleado) => ({
    type: REMOVE_EMPLEADO,
    payload: idEmpleado
})

/**Objetos para devolver al store */


/**
 * Metodo de ayuda para generar el correo basado en el nombre apellido y pais.
 * @param {objeto} empleado Informacion del empleado que se utiliza para generar el correo
 * @param {number} idEmpleado Id del empleado
 * @returns retorna el correo generado
 */
const crearEmail = async (empleado, idEmpleado = -1) => {
    const {
        empleado_primer_nombre: nombre,
        empleado_primer_apellido: apellido,
        pais_id
    } = empleado;

    const apellidoMap = apellido.toLocaleLowerCase().replaceAll(" ", "");

    const correo = await generateEmail({
        idEmpleado,
        nombre: nombre.toLocaleLowerCase(),
        apellido: apellidoMap,
        dominio: pais_id === "1" ? 'cidenet.com.co' : 'cidenet.com'
    })
    return correo;
}