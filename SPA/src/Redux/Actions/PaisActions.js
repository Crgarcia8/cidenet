import { GET_PAISES } from "../types";
import { getPaises } from '../../Services/PaisesService';

/**Consultar para obtener todos los Paises */
export const obtenerTodosPaises = () => async dispatch => {
    const response = await getPaises();
    return dispatch(recibirPaises(response.data));
}


/**Objetos para devolver al store */
const recibirPaises = (paises) => ({
    type: GET_PAISES,
    payload: paises
})
/**Objetos para devolver al store */