<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Empleados extends REST_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('empleados_model');
        header("Access-Control-Allow-Methods: GET, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
        if ( "OPTIONS" === $_SERVER['REQUEST_METHOD'] ) {
            die();
        }
        date_default_timezone_set("America/Bogota");
	}
    public function index_get()
    {
        $res = $this->empleados_model->getAll();
        $this->response($res, REST_Controller::HTTP_OK);
    }
    public function byId_get($idEmpleado)
    {
        $res = $this->empleados_model->getById($idEmpleado);
        $this->response($res, REST_Controller::HTTP_OK);
    }
    public function existeCorreo_post()
    {
        if (!$this->post("correo") || !$this->post("empleadoId")) {
            $this->response(array("error"=>"no existe información completa para consultar correo"), 400);
        }
        $existe = $this->empleados_model->verificarExisteCorreo($this->post("empleadoId"), $this->post("correo"));
        if ($existe){
            $this->response("Ya existe este correo",REST_Controller::HTTP_NOT_FOUND);
        }else{
            $this->response("No existe este correo",REST_Controller::HTTP_OK);
        }
    }
    function index_post()
    {
        if (!$this->post()) {
            $this->response(array("error"=>"no existe información para guardar"), 400);
        }        
        try{
            $empleado=$this->post();
            $res = $this->empleados_model->save($empleado);
            $this->response($res, REST_Controller::HTTP_OK);
        }catch(\Exception $e){
            $this->response($e->getMessage(), REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    function index_put($id)
    {
        if (!$this->put()||!$id) {
            $this->response(array("error"=>"no existe informacion para actualizar"), 400);
        }
        $empleado = $this->put();
        $empleado["empleado_fecha_actualizacion"]=date("Y-m-d H:i:s");
        try{
            $res = $this->empleados_model->update($id, $empleado);
            $this->response($res, REST_Controller::HTTP_OK);
        }catch(\Exception $e){
            $this->response($e->getMessage(), REST_Controller::HTTP_BAD_REQUEST);
        }
    }
    function index_delete($id)
    {
        if (!$id) {
            $this->response(array("error"=>"no existe variable 'id'"), 400);
        }
        try{
            $res=$this->empleados_model->delete($id);
            $this->response($res, REST_Controller::HTTP_OK);
        }catch(\Exception $e){
            $this->response($e->getMessage(), REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}