<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Paises extends REST_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('paises_model');
        header("Access-Control-Allow-Methods: GET, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
        if ( "OPTIONS" === $_SERVER['REQUEST_METHOD'] ) {
            die();
        }
	}
    public function index_get()
    {
        $res = $this->paises_model->getAll();
        $this->response($res, REST_Controller::HTTP_OK);// OK (200)
    }
}