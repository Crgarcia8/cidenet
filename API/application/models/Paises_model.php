<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Paises_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }

    public function getAll(){
        $query = $this->db->select('*')->from('tbl_paises')->get();
        return $query->result_array();
    }
}
?>