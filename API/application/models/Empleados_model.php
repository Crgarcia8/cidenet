<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empleados_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }

    public function getAll(){
        $query = $this->db->select('*')->from('vw_empleados')->get();
        return $query->result_array();
    }
    
    public function getById($id) {
        $query = $this->db->select('*')->from('tbl_empleados')->where('empleado_id',$id)->get();
        return $query->row();
    }
    public function verificarExisteCorreo($empleadoId, $correo){
        if ($empleadoId==-1){
            $where = "empleado_correo='" . $correo ."'";
        }else{
            $where = "empleado_correo='" . $correo . "' AND empleado_id!=".$empleadoId;
        }
        $query = $this->db->select('empleado_correo')->from('tbl_empleados')->where($where)->get();
        return $query->num_rows() > 0 ? true : false;
    }
    public function save($empleado) {
        $query = $this->db->set($empleado)->insert('tbl_empleados');
        if ($this->db->error()["code"] == 0){
            $maxId = $this->db->select_max('empleado_id')->from('tbl_empleados')->get();
            $maxId=$maxId->row();
            $maxId=(!$maxId->empleado_id)?1:$maxId->empleado_id;
            return $this->getById($maxId);
        }else{
            throw new \Exception($this->db->error()["message"]);
        }
    }

    public function update($id, $empleado)
    {
        $query = $this->db->set($empleado)->where('empleado_id', $id)->update('tbl_empleados');
        if ($this->db->error()["code"] == 0){
            return $this->getById($id);
        }else{
            throw new \Exception($this->db->error()["message"]);
        }
    }

    public function delete($id) {
        $query = $this->db->where('empleado_id', $id)->delete('tbl_empleados');
        if ($this->db->error()["code"] != 0){
            throw new \Exception($this->db->error()["message"]);
        }
    }

}
?>