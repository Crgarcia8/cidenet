<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| https://codeigniter.com/user_guide/general/routing.html
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;

/* Empleados*/
$route['empleados']['GET'] = 'empleados';
$route['empleados/(:num)']['GET'] = 'empleados/byId/$1';
$route['empleados']['POST'] = 'empleados';
$route['empleados/(:num)']['PUT'] = 'empleados/$1';
$route['empleados/(:num)']['DELETE'] = 'empleados/$1';
$route['empleados/existeCorreo']['POST'] = 'empleados/existeCorreo';

/* Paises*/
$route['paises']['GET'] = 'paises';

/* Tipos de Identificacion*/
$route['tiposDeIdentificacion']['GET'] = 'tiposDeIdentificacion';

/* Estados*/
$route['estadps']['GET'] = 'estados';

/* Areas*/
$route['areas']['GET'] = 'areas';
