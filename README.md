# Cidenet - Prueba Técnica

## Instructivo de instalación y configuración

### Base de Datos

La Base de Datos se encuentra en MySql, para lo cual los scripts de la creación de la Base de Datos y la estructura de datos se encuentra en la carpeta [Scripts](https://gitlab.com/Crgarcia8/cidenet/-/tree/main/Scripts)

Dentro de esta carpeta se encuentran 2 archivos

1. **Create Database.sql**: Con este se crea la Base de Datos db_cidenet y la estructura de tablas y vistas.
2. **Insert data.sql**: Este archivo tiene los scripts para insertar la data inicial.

### API

El API se realizó en [Codeigniter](https://codeigniter.com/), el cual es un framework en PHP, para lo cual se necesita xampp o IIS configurado para poder visualizarlo.

La configuración de la Base de Datos se encuentra dentro del API en la siguiente ruta: 

- API\application\config\database.php

#### Logs en el API

El manejo de los logs se realiza desde API ante cualquier error, se guarda automaticamente en la siguiente ruta: 

- API\application\logs\\[fecha-del-error].php

### SPA

El SPA se hizo en [React](https://es.reactjs.org/) con las siguientes librerias.

- Material-UI
- Material-UI/icons
- Axios
- Moment
- Notistack
- Redux

La ruta de configuración para la conexión con el API se encuentra en la siguiente ruta:

- SPA\src\http-common.js

### Videos de la aplicación funcionando

Se encuentran 2 videos realizados, se encuentran en la carpeta [videos](https://gitlab.com/Crgarcia8/cidenet/-/tree/main/Videos)

### Consideraciones finales

Por tiempo no logré terminar las siguientes funcionalidades:

- Filtrar los empleados por Estados, Tipos de Identificacion, País y Área.
- Cambiar los colores por defectos de la plantilla.
- El registro de ingreso y salida de empleados.

