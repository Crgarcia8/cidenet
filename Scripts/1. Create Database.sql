CREATE DATABASE db_cidenet;

USE db_cidenet;

CREATE TABLE tbl_areas (
  area_id int(11) NOT NULL AUTO_INCREMENT COMMENT 'Autonumérico del área',
  area_nombre varchar(100) NOT NULL COMMENT 'Nombre del Área',
  PRIMARY KEY (area_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

CREATE TABLE tbl_estados (
  estado_id int(11) NOT NULL AUTO_INCREMENT COMMENT 'Autonumérico del estado',
  estado_nombre varchar(100) NOT NULL COMMENT 'Nombre del estado',
  PRIMARY KEY (estado_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

CREATE TABLE tbl_paises (
  pais_id int(11) NOT NULL AUTO_INCREMENT COMMENT 'Autonumérico del País',
  pais_nombre varchar(100) NOT NULL COMMENT 'Nombre del país',
  PRIMARY KEY (pais_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

CREATE TABLE tbl_tipos_identificacion (
  tipo_identificacion_id int(11) NOT NULL AUTO_INCREMENT COMMENT 'Autonumérico del Tipo de Identificación',
  tipo_identificacion_nombre varchar(100) NOT NULL COMMENT 'Nombre del Tipo de Identificación',
  PRIMARY KEY (tipo_identificacion_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

CREATE TABLE tbl_empleados (
  empleado_id int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id unico autonumérico del empelado',
  empleado_primer_nombre varchar(20) NOT NULL COMMENT 'Primer nombre del empleado',
  empleado_otros_nombre varchar(50) DEFAULT NULL COMMENT 'Otros posibles nombres del empleado',
  empleado_primer_apellido varchar(20) NOT NULL COMMENT 'Primer apellido del empleado',
  empleado_segundo_apellido varchar(20) NOT NULL COMMENT 'Segundo apellido del empleado',
  pais_id int(11) DEFAULT NULL COMMENT 'Id del pais relacionado al empleado',
  tipo_identificacion_id int(11) DEFAULT NULL COMMENT 'id del tipo de documento del empleado',
  empleado_num_identificacion varchar(20) DEFAULT NULL COMMENT 'Texto del numero de documento del empleado(Alfanumérico)',
  empleado_correo varchar(300) NOT NULL COMMENT 'Correo del empleado(Único)',
  empleado_fecha_ingreso date DEFAULT NULL COMMENT 'Fecha de ingreso del empleado',
  area_id int(11) DEFAULT NULL COMMENT 'Id del area asociado al empleado',
  estado_id int(11) DEFAULT NULL COMMENT 'Id del estado del empleado(Activo, Inactivo)',
  empleado_fecha_registro datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Fecha y hora del registro del empleado(por defecto es fecha y hora actual)',
  empleado_fecha_actualizacion datetime DEFAULT NULL COMMENT 'Fecha y hora de la actualización del empleado',
  PRIMARY KEY (empleado_id),
  UNIQUE KEY unique_doc_tipo_documento (tipo_identificacion_id,empleado_num_identificacion),
  KEY tbl_empleados_ibfk_1 (area_id),
  KEY tbl_empleados_ibfk_3 (estado_id),
  KEY tbl_empleados_ibfk_4 (pais_id),
  CONSTRAINT tbl_empleados_ibfk_1 FOREIGN KEY (area_id) REFERENCES tbl_areas (area_id),
  CONSTRAINT tbl_empleados_ibfk_2 FOREIGN KEY (tipo_identificacion_id) REFERENCES tbl_tipos_identificacion (tipo_identificacion_id),
  CONSTRAINT tbl_empleados_ibfk_3 FOREIGN KEY (estado_id) REFERENCES tbl_estados (estado_id),
  CONSTRAINT tbl_empleados_ibfk_4 FOREIGN KEY (pais_id) REFERENCES tbl_paises (pais_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;


CREATE VIEW vw_empleados AS 
	SELECT 
	e.empleado_id,
	e.empleado_primer_nombre,
    e.empleado_otros_nombre,
    e.empleado_primer_apellido,
    e.empleado_segundo_apellido,
    e.pais_id,
    p.pais_nombre,
    e.tipo_identificacion_id,
	ti.tipo_identificacion_nombre,
    e.empleado_num_identificacion,
    e.empleado_correo,
    e.empleado_fecha_ingreso,
    e.area_id,
    a.area_nombre,
    e.estado_id,
    es.estado_nombre,
    e.empleado_fecha_registro,
	e.empleado_fecha_actualizacion
      FROM tbl_empleados e 
        JOIN tbl_areas a on a.area_id = e.area_id
        JOIN tbl_estados es on es.estado_id = e.estado_id
        JOIN tbl_paises p on p.pais_id = e.pais_id 
        JOIN tbl_tipos_identificacion ti on ti.tipo_identificacion_id = e.tipo_identificacion_id