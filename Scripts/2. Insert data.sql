/*Insertar areas*/
INSERT INTO tbl_areas (area_nombre)VALUES('Administración'), ('Financiera'), ('Compras'), ('Infraestructura'), ('Operación'), ('Talento Humano'), ('Servicios Varios');

/*Insertar Estados*/
INSERT INTO tbl_estados (estado_nombre)VALUES('Activo'),('Inactivo');

/*Insertar Paises*/
INSERT INTO tbl_paises (pais_nombre)VALUES('Colombia'),('Estados Unidos');

/*Insertar Tipos de Identificacion*/
INSERT INTO tbl_tipos_identificacion (tipo_identificacion_nombre)VALUES('Cédula de Ciudadanía'),('Cédula de Extranjería'),('Pasaporte'),('Permiso Especial');